# GenuaryBot

The Genuary Retweet Bot. It retweets things tagged with #genuary or #genuary2021 (or #genuary2022 etc.).

See the output at [@genuarybot](https://www.twitter.com/genuarybot).


Requires:

```
tweepy
configobj
loguru
```

The config file containing tokens should be called `config.ini` and look like:

```
# Config file for GenuaryBot
#############################

# aka consumer_key:
api_key = "APP-KEY"
# aka consumer_secret:
api_secret_key = "APP-SECRET"

# authorized with "twurl authors --consumer-key [api_key] --consumer-secret [api_secret_key]"
token = "YOUR-TOKEN"
secret = "YOUR-SECRET"
```

For now it runs in a `zsh` loop:

```zsh
while true; do python3 bot.py; sleep 300; done
```
