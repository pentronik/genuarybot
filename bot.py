"""Bot that retweets #genuary Tweets."""

import datetime
import sys
import time
import tweepy
from configobj import ConfigObj
from loguru import logger

QUERY = "#genuary OR #genuary2021 OR #genuary2022"
NOW = datetime.datetime.utcnow()

DEBUG_MODE = False  # If False, this script _will_ retweet.


def get_api(config_file="config.ini"):
    """Get the tweepy.API."""
    # Load the config values that are needed.
    config = ConfigObj("config.ini")

    # Get the Twitter API.
    auth = tweepy.OAuthHandler(config["api_key"], config["api_secret_key"])
    auth.set_access_token(config["token"], config["secret"])
    api = tweepy.API(auth)
    return api


def get_minutes_ago(status):
    """Returns how many minutes ago the Tweet was created at."""
    return (NOW - status.created_at).seconds / 60


def get_url(status):
    """Returns the string of the url to the Tweet."""
    return (f"https://www.twitter.com/{status.user.screen_name}"
            f"/status/{status.id}")


def sorted_on_id(list_of_tweets):
    return sorted(list_of_tweets, key=lambda _: _.id)


# Gets most recent tweet on @genuary_bot's timeline.
def get_recent_tweet(api, n=5):
    """Get's the most recent Tweet on the logged in user's home timeline."""
    # Get's the 5th oldest retweet so we catch anything that was missed the
    # last time we ran.
    latest = sorted_on_id(api.home_timeline(count=n, include_rts=True))[0]
    delta_minutes = get_minutes_ago(latest)
    logger.info(f"The {n}th most resent Tweet is {latest.id} from "
                f"{delta_minutes:0.1f} minutes ago.")
    logger.debug(f"  {get_url(latest)}")
    return latest


def retweet_carefully(api, tweet_id):
    """Retweet with try."""
    try:
        api.retweet(tweet_id)
    except tweepy.error.TweepError as e:
        logger.exception(e)
    except tweepy.error.RateLimitError as e:
        logger.exception(e)
        logger.critical("We hit the rate limit, so exiting.")
        sys.exit()


def valid_for_retweeting(api, tweet):
    """Checks that a tweet is valid for retweeting."""
    updated = api.get_status(tweet.id, include_my_retweet=True)
    status = True
    if hasattr(updated, "retweeted_status"):
        logger.debug(f"Skipping this retweet: {get_url(updated)}.")
        status = False
    if hasattr(updated, "current_user_retweet"):
        logger.debug(f"Not going to retweet {get_url(updated)} since I "
                     "already did that.")
        status = False
    return status


def send_updates_since_id(api, tweet_id):
    """Retweet all the new Tweets that have been created since the argument."""
    retweet_count = 0
    all_collected_tweets = []
    while True:
        tweets = api.search(QUERY, since_id=tweet_id, result_type="recent",
                            count=100)
        logger.info(f"Got {len(tweets)} Tweets from query.")
        if not tweets:
            logger.debug("No more Tweets from query, so breaking the loop.")
            break
        for tweet in sorted_on_id(tweets):
            if not valid_for_retweeting(api, tweet):
                continue
            logger.info(f"Retweeting id = {tweet.id} by "
                        f"{tweet.user.screen_name} from "
                        f"{get_minutes_ago(tweet):0.1f} minutes ago.")
            retweet_count += 1
            if not DEBUG_MODE:
                # Not in debug mode, so actually going to send these.
                retweet_carefully(api, tweet.id)
                time.sleep(5)  # Don't send them too fast.
        tweet_id = tweet.id  # Update tweet id to the most recent tweet.
        all_collected_tweets.extend(tweets)
    logger.info(f"Retweeted {retweet_count} Tweets this time.")
    return all_collected_tweets


if __name__ == "__main__":
    api = get_api()
    # Get the 5th most recent Tweet.
    cursor = get_recent_tweet(api)
    logger.info(f"Checking for new '{QUERY}' Tweets since "
                f"{get_minutes_ago(cursor)} minutes ago.")
    # Retweet anything new since the 5th most recent tweet on @genuarybot's
    # timeline.
    all_processed_tweets = send_updates_since_id(api, cursor.id)
    logger.info(f"Exiting.")
